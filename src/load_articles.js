$(document).ready()
{
    var title = $('.title');
    var articles = $('.article');
    var image = $('.img');

    $.ajax({
        url: 'https://sporadic.nz/2018a_web_assignment_service/Articles',
        type: 'GET',
        dataType: 'json',
        success: function (arg) {
            title.each(function (index) {
                if (index != 5){
                    $(this).text("");
                    $(this).append(arg[index].title);
                }
            });
            articles.each(function(index){
                if (index!=5){
                    $(this).text("");
                    $(this).append(arg[index].content);
                }
            });

            image.each(function(index){
                if (index!=5){
                    $(this).attr('src', arg[index].imageUrl);
                }
            });

        }
    })
}
